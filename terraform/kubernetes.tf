terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
}

provider "kubernetes" {}

# apiVersion: rbac.authorization.k8s.io/v1
# kind: Role
# metadata:
#   namespace: default
#   name: pod-reader
# rules:
# - apiGroups: [""] # "" indicates the core API group
#   resources: ["pods"]
#   verbs: ["get", "watch", "list"]

resource "kubernetes_role" "operador" {
  metadata {
    namespace = "production"
    name      = "operador"
  }
  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
}