#!/bin/bash
ns=$1

ns=`kubectl get namespace $1 --no-headers --output=go-template={{.metadata.name}} 2>/dev/null`
if [ -z "${ns}" ]; 
then
  echo "Namespace no existe"
  kubectl create namespace $1
else
  echo "Namespace ya existe"
fi
  

