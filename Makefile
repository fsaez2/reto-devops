.PHONY: help

RED          := $(shell tput -Txterm setaf 1)
GREEN        := $(shell tput -Txterm setaf 2)

RESET := $(shell tput -Txterm sgr0)


help:
	@echo "Escribir el reto que se debe realizar. Ej: make reto2"

reto1:
	@echo "${GREEN}Reto 1: Dockerizar imagen${RESET}"
	docker build --pull --rm -f "web/Dockerfile" -t retodevops:latest "web"
	@echo "${GREEN}Reto 1: Imagen construida. Se ejecutará${RESET}"
	docker run --rm -d  -p 3000:3000/tcp retodevops:latest
	@echo "${GREEN}Ingresar a http://127.0.0.1:3000${RESET}"

reto2:
	@echo "Reto 2"
	docker-compose -f "docker-compose.yml" up -d --build 
	@echo "${GREEN}Ingresar a http://127.0.0.1"
	@echo "Para ingresar a http://127.0.0.1/private usar:"
	@echo "Usuario: user"
	@echo "Contraseña: T333.P44${RESET}"  

clean:
	@echo "Reto 1: Deteniendo imagen de Docker ejecutada"
	-docker stop $(shell docker ps -a -q --filter ancestor=retodevops:latest --format="{{.ID}}")
	@echo "Reto 2: Deteniendo imagen de docker-compose"
	-docker-compose -f "docker-compose.yml" down 