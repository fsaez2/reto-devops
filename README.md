# Reto-DevOps CLM
![CLM Consoltores](./web/app/img/clm.png)

## Reto 1

La imágen se encuentra dockerizada, utilizando la imagen de node:alpine debido a su menor tamaño y se ejecuta con el usuario 'app' en el directorio interno '/usr/app'

Construcción y Ejecución manual: 

```console
docker build --pull --rm -f "web/Dockerfile" -t retodevops:latest "web"
docker run --rm -d  -p 3000:3000/tcp retodevops:latest
```

Utilizando Makefile:
```console
make reto1
```

## Reto 2

Se creó el docker-compose.yaml el cual contiene la aplicación actual y NGINX, donde se incorporó la imagen nginx:1.19-alpine y en la construcción se crea un certificado autogenerado, además del usuario y contraseña para la protección de /protect. Los valores de usuario y contraseña se pueden modificar en el archivo docker-compose.yml.

Este NGINX contiene el template para redirigir el tráfico HTTP a HTTPS de forma permanente, además de proteger el endpoint /protect

Construcción y Ejecución manual: 
```console
docker-compose -f "docker-compose.yml" up -d --build 
```

Utilizando Makefile:
```console
make reto2
```

## Reto 3 al 6

Los retos que van del 3 al 6 se juntaron debido a que todos se encuentran en un solo Pipeline.  Para esto se utilizó el CI/CD de Gitlab, por lo que se encuentra en el archivo gitlab-ci.yml. Éste cuenta con tres stages: build, test, deploy y se desencadena cuando se realiza un tag.
Las tareas dentro de esta pipeline son las siguientes:

### Reto 3
#### build-web-docker
En este trabajo se utiliza DinD (Docker-in-Docker) y se encarga de construir, taggear, loguearse al Registry y subir imágen a éste.

Variables necesarias
* CI_REGISTRY_IMAGE: Repositorio donde se encuentra este código. Actual: registry.gitlab.com/fsaez2/reto-devops
* CI_REGISTRY: Sitio de donde se encuentra el repositorio. Actual: registry.gitlab.com
* CI_COMMIT_REF_NAME: Variable automática. Es el branch donde se realiza el deploy.
* CI_JOB_TOKEN: Variable automática. Contiene token para autenticación.

#### build-nginx-docker
En este trabajo se utiliza DinD (Docker-in-Docker) y se encarga de construir, taggear, loguearse al Registry y subir imágen a éste.

Variables necesarias
* BARG_HOSTNAME: Argumento para la construcción de la imagen de Docker, corresponde a Hostname a utilizar para NGINX.
* BARG_PRIVATE_API_USER: Argumento para la construcción de la imagen de Docker, corresponde al usuario del endpoint /private.
* BARG_PRIVATE_API_PASSWD: Argumento para la construcción de la imagen de Docker, corresponde a la contraseña del endpoint /private.
* CI_REGISTRY_IMAGE: Repositorio donde se encuentra este código. Actual: registry.gitlab.com/fsaez2/reto-devops
* CI_REGISTRY: Sitio de donde se encuentra el repositorio. Actual: registry.gitlab.com
* CI_COMMIT_REF_NAME: Variable automática. Es el branch donde se realiza el deploy.
* CI_JOB_TOKEN: Variable automática. Contiene token para autenticación.

#### test-web-docker
En este trabajo se utiliza DinD (Docker-in-Docker) y se encarga de ejecutar las pruebas de la aplicación realizada.

Variables necesarias
* CI_REGISTRY_IMAGE: Repositorio donde se encuentra este código. Actual: registry.gitlab.com/fsaez2/reto-devops
* CI_COMMIT_REF_NAME: Variable automática. Es el branch donde se realiza el deploy.

### Reto 4
#### deploy-to-gke
En este trabajo se utiliza la imagen de google/cloud-sdk debido a que se utilizó Google Kubernetes Engine (GKE). Se encarga de configurar el ambiente de gcloud, identificarse y obtener las credenciales para recuperar las imágenes del Registry. Además utiliza Kustomize para obtener el tag actualizado a deployar. Por último, realiza el deploy de la imagen y además le aplica los parámetros para HorizontalPodAutoscaler.

Se requiere la creación de un Cluster en GKE, además de generar un Deploy Token de Gitlab desde la sección Settings -> Repository -> Deploy Tokens


Variables necesarias
* GKE_KEY: Llave JSON obtenida desde la cuenta de servicios creada en GKE con los permisos de Kubernete Cluster Admin.
* GCP_PROJECT: ID Proyecto de Google Cloud.
* GKE_CLUSTER_NAME: Nombre del Cluster de GKE.
* GKE_CLUSTER_ZONE: Zona donde se encuentra el Cluster de GKE.
* CI_REGISTRY_USER: Usuario creado desde el Deploy Token de Gitlab. 
* CI_REGISTRY_TOKEN: Token generado desde el Deploy Token de Gitlab.
* CI_REGISTRY_IMAGE: Repositorio donde se encuentra este código. Actual: registry.gitlab.com/fsaez2/reto-devops
* CI_COMMIT_REF_NAME: Variable automática. Es el branch donde se realiza el deploy.

Anexo 1: Creación de Cluster en GKE. (TODO: Automatizar con Terraform)
```console
export my_zone=zona-gke
export my_cluster=nombre-cluster
gcloud container clusters create $my_cluster --num-nodes 3 --enable-ip-alias --zone $my_zone
```

Anexo 2: Crear cuenta de servicio y obtener llave JSON:
- Crear una cuenta de <a href="https://console.cloud.google.com/iam-admin/serviceaccounts">Servicio en Google Cloud</a>
- Asignar los siguientes roles de Cloud IAM:
- Kubernetes Engine Developer
-- Storage Admin
- Crear un secreto JSON de la cuenta de servicio. 

### Reto 5
#### deploy-to-gke-helm
Este trabajo utiliza la misma metodología seguida en el Reto 4, sin embargo, el deployment se realiza a través de un Chart de Helm el cual se creó para soportar una versión propia de la configuración de NGINX, sumado al sitio en node. Adicionalmente utiliza un nginx-ingress como endpoint principal. Se crea en un namespace distinto al 'default', e instala Helm en el servidor del job para operar con los Charts.

Variables necesarias
* GKE_KEY: Llave JSON obtenida desde la cuenta de servicios creada en GKE con los permisos de Kubernete Cluster Admin.
* GCP_PROJECT: ID Proyecto de Google Cloud.
* GKE_CLUSTER_NAME: Nombre del Cluster de GKE.
* GKE_CLUSTER_ZONE: Zona donde se encuentra el Cluster de GKE.
* CI_REGISTRY_USER: Usuario creado desde el Deploy Token de Gitlab. 
* CI_REGISTRY_TOKEN: Token generado desde el Deploy Token de Gitlab.
* CI_REGISTRY_IMAGE: Repositorio donde se encuentra este código. Actual: registry.gitlab.com/fsaez2/reto-devops
* CI_COMMIT_REF_NAME: Variable automática. Es el branch donde se realiza el deploy.

### Reto 6
#### terraform-create
Este trabajo también utiliza la imagen google/cloud-sdk del Reto 4 y 5, sin embargo instala Terraform en el servidor del job y aplica la configuración requerida referente a crear un rol (llamado operador) que solo puede ver los pods que están en el namespace 'production', utilizado en el Reto 5.

## Reto 7
Para el último reto, se creó un Makefile que maneja el reto 1 y reto 2, debido a que se ejecutan de forma local y no en un pipeline de Gitlab.

Ejecutar Reto 1:
```console
make reto1
```

Ejecutar Reto 2:
```console
make reto2
```

Detener retos:
```console
make clean
```

## Despliegue actual
Para efectos de demostración para esta entrega, se dejó desplegado el tag <a href="https://gitlab.com/fsaez2/reto-devops/-/commits/v0.0.8">v0.0.8</a> que utilizó el pipeline <a href="https://gitlab.com/fsaez2/reto-devops/-/pipelines/216160909">#216160909</a> en GKE con los siguientes datos:

### Pods desplegados en default
```console
[fsaez@xpstwo ~]$ kubectl get pod
NAME                                                   READY   STATUS    RESTARTS   AGE
nginx-ingress-ingress-nginx-controller-9947c7f-smdzz   1/1     Running   0          33m
web-77c9c54845-7mjjg                                   1/1     Running   0          33m
```

### Pods desplegados en production
```console
[fsaez@xpstwo ~]$ kubectl get pod --namespace=production
NAME                                       READY   STATUS    RESTARTS   AGE
retoapp-nginx-deployment-9bd58d7f4-6blsq   1/1     Running   0          33m
retoapp-web-deployment-7d547d899b-tdsdl    1/1     Running   0          33m
```

### Ingress que recibe peticiones
```console
[fsaez@xpstwo ~]$ kubectl get ingress --namespace=production
NAME                 HOSTS   ADDRESS          PORTS   AGE
production-ingress   *       35.223.164.171   80      34m
```

### Listado de Helm
```console
[fsaez@xpstwo ~]$ helm list
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                   APP VERSION
nginx-ingress   default         1               2020-11-14 20:55:35.659139122 +0000 UTC deployed        ingress-nginx-3.10.1    0.41.2     
retoapp         default         1               2020-11-14 20:55:28.563874639 +0000 UTC deployed        retoapp-0.1.1           0.0.7      
```

### Rol en namespace
```console
[fsaez@xpstwo ~]$ kubectl get role -n production
NAME       AGE
operador   35m21s
[fsaez@xpstwo ~]$ kubectl describe role -n production
Name:         operador
Labels:       <none>
Annotations:  <none>
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  pods       []                 []              [list watch get]
```

### Prueba de conexión (Se mantendrá activo y con acceso para todos)
Conexión a endpoint público con redirección
```console
[fsaez@xpstwo ~]$ curl http://35.223.164.171/public -L -v -k
*   Trying 35.223.164.171:80...
* Connected to 35.223.164.171 (35.223.164.171) port 80 (#0)
> GET /public HTTP/1.1
> Host: 35.223.164.171
> User-Agent: curl/7.71.1
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 308 Permanent Redirect
< Date: Sat, 14 Nov 2020 21:30:48 GMT
< Content-Type: text/html
< Content-Length: 164
< Connection: keep-alive
< Location: https://35.223.164.171/public
< 
* Ignoring the response-body
* Connection #0 to host 35.223.164.171 left intact
* Issue another request to this URL: 'https://35.223.164.171/public'
*   Trying 35.223.164.171:443...
* Connected to 35.223.164.171 (35.223.164.171) port 443 (#1)
...
> GET /public HTTP/2
> Host: 35.223.164.171
> user-agent: curl/7.71.1
> accept: */*
> 
* Connection #1 to host 35.223.164.171 left intact
{"public_token":"12837asd98a7sasd97a9sd7"}
```

Endpoint protegido
```console
[fsaez@xpstwo ~]$ curl http://35.223.164.171/private -L -k
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.19.4</center>
</body>
</html>

[fsaez@xpstwo ~]$ curl http://35.223.164.171/private -u user:T333.P44 -L -k
{"private_token":"TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="}
```

## TODO
Como siempre existe espacio para mejorar, a continuación describo puntos que se podrían agregar para mejorar los resultados de este reto:

* Automatizar con Terraform la creación de Cluster GKE (o minukube).
* Automatizar la creación de cuenta de servicio, agregar permisos y la exportación de JSON.
* Agregar la obtención de dirección IP estática global para Cluster GKE, además de DNS y obtener certificado Let's Encrypt.
* Automatizar la eliminación del ambiente, que actualmente corresponde a:
* - helm uninstall retoapp
* - helm uninstall nginx-ingress
* - kubectl delete role operador -n production
* - make clean
* Parametrizar mayores variables en los Charts.
